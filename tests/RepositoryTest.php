<?php

/**
 * @property VacancyRepository repo
 * @property mixed             sampleData
 */
class RepositoryTest extends TestCase
{
    /**
     * Setup the test environment.
     */
    public function setUp()
    {
        parent::setUp();

        $this->repo = new \App\Repositories\VacancyRepository();
        $this->sampleData = json_decode(app()->files->get(storage_path('app/small_mock_data.json')));
    }

    /**
     * Test the available method.
     */
    public function testAvailableTrue()
    {
        $this->assertTrue($this->repo->connection('api1')->available(), 'The repo is available');
    }

    /**
     * Test what happens when a connection that doesn't exist is selected.
     */
    public function testAvailableFalse()
    {
        $this->setExpectedException(\App\Exceptions\ConnectionDoesNotExist::class);
        $this->repo->connection('api5')->available();
    }

    /**
     * Test fromAll() method.
     */
    public function testFromAll()
    {
        $this->assertTrue(is_array($this->repo->fromAll()), 'The returned value is an array.');
    }

    /**
     * Test getResponse() method.
     */
    public function testGetResponse()
    {
        $this->assertTrue(is_array($this->repo->getResponse()) || is_object($this->repo->getResponse()), 'The returned value is an array or an object.');
    }

    /**
     * Test all() method.
     */
    public function testAll()
    {
        $this->assertTrue(is_array($this->repo->all()), 'The returned value is an array.');
    }

    /**
     * Test single() method.
     */
    public function testSingle()
    {
        $this->assertTrue(is_object($this->repo->single(1)), 'The returned value is an object.');
        $this->assertTrue(isset($this->repo->single(1)->title), 'The returned value has a title property.');
    }
}
