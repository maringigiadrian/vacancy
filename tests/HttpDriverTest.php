<?php

/**
 * @property VacancyRepository            repo
 * @property mixed                        sampleData
 * @property array                        badConnection
 * @property \App\Repositories\HttpDriver badRepo
 */
class HttpDriverTest extends TestCase
{
    /**
     * Setup the test environment.
     */
    public function setUp()
    {
        parent::setUp();

        $this->repo = new \App\Repositories\HttpDriver(config('connections.connections.api1'));
        $this->sampleData = json_decode(app()->files->get(storage_path('app/small_mock_data.json')));
        $this->badConnection = [
            'driver' => App\Repositories\HttpDriver::class,
            'path' => 'http://vacancy.app/api/bad-url',
            'data-path' => ['hidden', 'data'],
        ];
        $this->badRepo = new \App\Repositories\HttpDriver($this->badConnection);
    }

    public function testAvailable()
    {
        $this->assertTrue($this->repo->available());
        $this->setExpectedException(\App\Exceptions\ConnectionNotAvailable::class);
        $this->badRepo->available();
    }

    public function testGetResponse()
    {
        $this->assertTrue(is_object($this->repo->getResponse()) || is_array($this->repo->getResponse()), 'The returned value is an array or an object.');

        $this->setExpectedException(\App\Exceptions\ConnectionNotAvailable::class);
        $this->badRepo->getResponse();
    }

    public function testExtractTheData()
    {
        $data = [
            'hidden' => [
                'data' => [
                    1,
                    2,
                    3,
                    4,
                ],
            ],
        ];

        $this->assertTrue([1, 2, 3, 4] == $this->badRepo->extractTheData($data));
        $this->assertFalse([4, 2, 3, 1] == $this->badRepo->extractTheData($data));
    }
}
