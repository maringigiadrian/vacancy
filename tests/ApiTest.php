<?php

class ApiTest extends TestCase
{
    /**
     * A basic test example.
     */
    public function testApi1()
    {
        $response = $this->call('GET', 'api1');

        $this->assertEquals(200, $response->getStatusCode());
        $this->assertTrue(is_object(json_decode($response->getContent())), 'Result is an object');
        $this->assertTrue(json_decode($response->getContent())->success, 'Response returned success');
    }

    public function testApi2()
    {
        $response = $this->call('GET', 'api2');

        $this->assertEquals(200, $response->getStatusCode());
        $this->assertTrue(is_array(json_decode($response->getContent())), 'Result is an array');
    }

    public function testApi3()
    {
        $response = $this->call('GET', 'api3');

        $this->assertEquals(200, $response->getStatusCode());
        $this->assertTrue(is_object(json_decode($response->getContent())), 'Result is an object');

        $this->assertTrue(json_decode($response->getContent())->success, 'Response returned success');
        $this->assertTrue(is_object(json_decode($response->getContent())->data), 'Result is an object');
        $this->assertTrue(is_array(json_decode($response->getContent())->data->vacancies), 'Result is an array');
    }
}
