<?php

/**
 * A list of available connections to third party APIs.
 */

return [

    'default' => 'api1',
    'connections' => [

        'api1' => [
            'driver' => App\Repositories\HttpDriver::class,
            'path' => 'http://vacancy.app/api1',
            'data-path' => ['data'], // how/where is the data stored in the response
        ],
        'api2' => [
            'driver' => App\Repositories\HttpDriver::class,
            'path' => 'http://vacancy.app/api2',
            'data-path' => null, // how/where is the data stored in the response
        ],
        'api3' => [
            'driver' => App\Repositories\HttpDriver::class,
            'path' => 'http://vacancy.app/api3',
            'data-path' => ['data', 'vacancies'], // how/where is the data stored in the response
        ],

    ],

];
