<?php

namespace app;

use Illuminate\Database\Eloquent\Model;

/**
 * A test vacancy model.
 */
class Vacancy extends Model
{
    protected $table = 'vacancies';

    /**
     * The id of the vacancy.
     *
     * @var int
     */
    public $id;

    /**
     * The vacancy title.
     *
     * @var string
     */
    public $title;

    /**
     * The vacancy content/description.
     *
     * @var string
     */
    public $content;

    /**
     * The vacancy description.
     *
     * @var string
     */
    public $description;
}
