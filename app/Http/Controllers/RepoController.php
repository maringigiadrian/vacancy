<?php

namespace App\Http\Controllers;

use App\Repositories\VacancyRepository;

class RepoController extends Controller
{
    /**
     * @var RepositoryInterface
     */
    private $repo;

    /**
     * RepoController constructor.
     */
    public function __construct()
    {
        $this->repo = new VacancyRepository('api1');
    }

    /**
     * Check to see if the connection is available.
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function available()
    {
        return response()->json([
            'success' => $this->repo->available(),
        ]);
    }

    /**
     * Get the full response from the connection.
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function response()
    {
        return response()->json($this->repo->getResponse());
    }

    /**
     * Get all vacancies from the selected connection.
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function all()
    {
        return response()->json($this->repo->all());
    }

    /**
     * Get all vacancies from all connections.
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function fromAll()
    {
        return response()->json($this->repo->fromAll());
    }

    /**
     * Geting one vacancy.
     *
     * @param int $id
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function single($id)
    {
        $response = $this->repo->single($id);

        return response()->json($response);
    }
}
