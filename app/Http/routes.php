<?php

$app->get('/', function () use ($app) {
    return $app->welcome();
});

$app->get('vacancies', function () use ($app) {
    return $app->make('App\Vacancy')
               ->all();
});


$app->get('repo/available', 'RepoController@available');
$app->get('repo/response', 'RepoController@response');
$app->get('repo/all', 'RepoController@all');
$app->get('repo/from-all', 'RepoController@fromAll');
$app->get('repo/single/{id}', 'RepoController@single');

/**
 * Setting up routes to simulate different APIs
 */

$app->get('api1', function () use ($app) {

    $data = json_decode($app->files->get(storage_path('app/mock_data.json')));

    return response()->json([
        'success' => true,
        'data' => $data,
    ]);

});

$app->get('api1/{id}', function ($id) use ($app) {

    $data = json_decode($app->files->get(storage_path('app/mock_data.json')));

    $data = $data[$id];

    return response()->json([
        'success' => true,
        'data' => $data,
    ]);

});

$app->get('api2', function () use ($app) {

    $data = json_decode($app->files->get(storage_path('app/mock_data.json')));

    return response()->json($data);

});

$app->get('api2/{id}', function ($id) use ($app) {

    $data = json_decode($app->files->get(storage_path('app/mock_data.json')));

    $data = $data[$id];

    return response()->json($data);

});

$app->get('api3', function () use ($app) {

    $data = json_decode($app->files->get(storage_path('app/small_mock_data.json')));

    return response()->json([
        'success' => true,
        'data' => [
            'vacancies' => $data,
        ],
    ]);

});
