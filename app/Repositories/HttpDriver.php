<?php

namespace App\Repositories;

use App\Exceptions\ConnectionNotAvailable;
use App\Exceptions\DataIsInvalid;

class HttpDriver implements VacancyRepositoryInterface
{
    private $connection;

    /**
     * HttpDriver constructor.
     *
     * @param $connection
     */
    public function __construct($connection = null)
    {
        if (!is_null($connection)) {
            $this->connection = $connection;
        } else {
            $defaultConnection = config('connections.default');
            $this->connection = config("connections.connections.{$defaultConnection}");
        }
    }

    /**
     * Check if the connection is available.
     *
     * @return mixed
     *
     * @throws ConnectionNotAvailable
     */
    public function available()
    {
        $path = $this->connection['path'];

        // trying to reach the path
        if (@file_get_contents($path) != false) {
            return true;
        }

        throw new ConnectionNotAvailable();
    }

    /**
     * Get the full response from the connection.
     *
     * @return object
     *
     * @throws ConnectionNotAvailable
     */
    public function getResponse()
    {
        $path = $this->connection['path'];

        if ($this->available()) {
            return json_decode(file_get_contents($path));
        }
    }

    /**
     * Checking to see if the data we received is valid.
     *
     * @param $data
     *
     * @return bool
     */
    public function dataIsValid($data)
    {
        return is_array($data);
    }

    /**
     * Separating the data from the response.
     *
     * @param $data
     *
     * @return mixed
     */
    public function extractTheData($data)
    {
        $path = $this->connection['data-path'];

        if (is_null($path)) {
            return $data;
        }

        foreach ($path as $p) {
            $data = (array) $data;
            $data = (array) $data[$p];
        }

        return $data;
    }

    /**
     * Get all items.
     *
     * @return mixed
     *
     * @throws DataIsInvalid
     */
    public function all()
    {
        $response = $this->getResponse();
        $data = $this->extractTheData($response);

        if ($this->dataIsValid($data)) {
            return $data;
        }

        throw new DataIsInvalid();
    }

    /**
     * Select one vacancy. Right now it's selected based
     * on the key of the array. More elaborated
     * selections may be implemented.
     *
     * @param int $id
     *
     * @return mixed
     */
    public function single($id)
    {
        $response = $this->getResponse();
        $data = $this->extractTheData($response);

        return $data[$id];
    }
}
