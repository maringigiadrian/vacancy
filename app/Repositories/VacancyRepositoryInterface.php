<?php

namespace App\Repositories;

interface VacancyRepositoryInterface
{
    /**
     * Check if the connection is available.
     *
     * @return mixed
     */
    public function available();

    /**
     * Get the full response from the connection.
     *
     * @return object
     */
    public function getResponse();

    /**
     * Get all items.
     *
     * @return mixed
     */
    public function all();

    /**
     * Get specific item.
     *
     * @param int $id
     *
     * @return mixed
     */
    public function single($id);

    /**
     * Checking to see if the data we received is valid.
     *
     * @param $data
     *
     * @return bool
     */
    public function dataIsValid($data);

    /**
     * Separating the data from the response.
     *
     * @param $data
     *
     * @return mixed
     */
    public function extractTheData($data);
}
