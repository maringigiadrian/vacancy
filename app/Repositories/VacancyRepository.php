<?php

namespace App\Repositories;

use App\Exceptions\ConnectionDoesNotExist;

class VacancyRepository
{
    private $repo;

    /**
     * @param null|string $connection
     *
     * @throws ConnectionDoesNotExist
     */
    public function __construct($connection = null)
    {
        if (is_null($connection)) {
            $connection = config('connections.default');
        }

        if ($this->connectionExists($connection)) {
            $this->setConnection($connection);
        }
    }

    /**
     * Checking to see if there is a configured connection by that name.
     *
     * @param string $connection
     *
     * @return bool
     *
     * @throws ConnectionDoesNotExist
     */
    public function connectionExists($connection)
    {
        if (array_key_exists($connection, config('connections.connections'))) {
            return true;
        }

        throw new ConnectionDoesNotExist();
    }

    /**
     * Setting the connection.
     *
     * @param string $connection
     *
     * @throws ConnectionDoesNotExist
     */
    protected function setConnection($connection)
    {
        if (!$this->connectionExists($connection)) {
            throw new ConnectionDoesNotExist();
        }

        $connection = config('connections.connections')[$connection];

        app()->bind(\App\Repositories\VacancyRepositoryInterface::class, function ($app) use ($connection) {
            return new $connection['driver']($connection);
        });

        $this->repo = app()->make(\App\Repositories\VacancyRepositoryInterface::class);
    }

    /**
     * This method allows you to change to another connection.
     *
     * @param string $connection
     *
     * @return $this
     */
    public function connection($connection)
    {
        if ($this->connectionExists($connection)) {
            $this->setConnection($connection);
        }

        return $this;
    }

    /**
     * Just in case you need the data from more than just one source
     *
     * @param $connections
     *
     * @return array
     * @throws ConnectionDoesNotExist
     */
    public function fromMore($connections)
    {
        $response = [];
        foreach ($connections as $connection => $data) {
            if ($this->connectionExists($connection)) {
                $this->setConnection($connection);
                $response = array_merge($response, $this->all());
            }
        }

        return $response;
    }

    /**
     * Get the data from all the sources
     *
     * @return array
     */
    public function fromAll()
    {
        $connections = config('connections.connections');

        return $this->fromMore($connections);
    }

    /**
     * Check if the service is up
     *
     * @return mixed
     */
    public function available()
    {
        return $this->repo->available();
    }

    /**
     * Get the full response
     *
     * @return mixed
     */
    public function getResponse()
    {
        return $this->repo->getResponse();
    }

    /**
     * Get all items
     *
     * @return array
     */
    public function all()
    {
        return $this->repo->all();
    }

    /**
     * Get one item
     *
     * @param $id
     *
     * @return mixed
     */
    public function single($id)
    {
        return $this->repo->single($id);
    }
}
