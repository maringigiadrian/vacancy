<?php

namespace App\Exceptions;

use Exception;

class ConnectionDoesNotExist extends Exception
{
}
