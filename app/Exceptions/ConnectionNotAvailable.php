<?php

namespace App\Exceptions;

use Exception;

class ConnectionNotAvailable extends Exception
{
}
