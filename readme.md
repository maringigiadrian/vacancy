## The Vacancy Repository problem

Installation

 * clone
 * run `composer install`
 * create a `.env` file
 * set-up at `http://vacancy.app/` or update the paths in `config/connections.php`

As Jeroen said the assignment is a bit vague so I fumbled a bit until I figured out what I needed to do.

I chose Laravel Lumen as a framework. I thought this module could work well as a micro-service. 

### Connections
In the `config` folder there is a `connections.php` file where you could easily add more connections.
Each connection item has a *driver*, represented through a class that handles all the communication with the API/service, a *path*, which instructs the driver where to call the API and a *data-path* which instructs the driver where to pick up the data (vacancies).

### Routes
The app has 5 main routes that lead to the `RepoController` where the `VacancyRepository` is called and interacted with. 

Also there are three routes used for simulating a service/API.

### The controller
In the `RepoController` the repository is instantiated with the `api3` connection. This can be changed with any other connection form the configuration list. Also it can be changed at runtime with the `connection` method (`$this->repo->connection('api1')->all()`).
Also the controller holds some basic interactions that you can do to the repository.

### The interface
In the `app/repositories` folder there is `VacacyRepositoryInterface` in which the main methods are declared. This allows the developer to write different implementations for this interface that adhere to the same contract.

### The HttpDriver
At the moment I made an http driver that gets the data through a get request. Pretty simple stuff.
in the constructor you may pass a connection. If not, the one set as default in the configuration file will be taken into consideration.

### The Repository
In the same folder there is the `VacancyRepository` wich makes the communication with the API/service possible.
The methods for interacting with the repository are

 * `available()` - see if the source is available or not.
 * `all()` - get all vacancies.
 * `single($id)` - get one vacancy. Right now it gets the current key of the array. Other logic could be inserted there.
 * `fromAll()` - get the vacancies from all of the sources.
 * `fromMore($connections)` - get the vacancies from more than just one connection.
 * `connection()` - change the connection on the fly.
 * `getResponse()` - get the full response from the service.
 
### The tests
There are three tests. 

* `ApiTest` - which tests the dummy routes
* `HttpDriverTest` - which tests the http driver
* `RepositoryTest` - which tests the repository methods 

Please be gentle :). I haven't done too much testing in the past.

*Thank you.*